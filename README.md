Enables clangd autocompletion with scons.

Requires SCons >= v4.0

Add the following to your SConscript:
```
# compilation db
env.Tool('compilation_db')
cdb = env.CompilationDatabase()
Alias('cdb', cdb)
```
this will generate the appropriate compile_commands.json required by clangd to generate on the fly autocompletion

copy the vim folder to you project as .vim
vimset script is to set the autocompletion to use the correct build target
